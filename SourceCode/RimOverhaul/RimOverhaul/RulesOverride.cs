﻿using Harmony;
using RimWorld;
using System;
using UnityEngine;
using Verse;
using System.Collections.Generic;
using RimWorld.Planet;
using System.Linq;

namespace RimOverhaul
{
    internal class RulesOverride : GameComponent
    {
        [HarmonyPatch(typeof(Page_CreateWorldParams), "DoWindowContents", new Type[]
        {
            typeof(Rect)
        })]
        private static class Page_CreateWorldParams_DoWindowContents_Patch
        {
            private static Vector2 planetInfoSliderPos;

            private static bool Prefix(Rect rect, ref Rect __state)
            {
                __state = rect;
                return true;
            }

            private static void Postfix(ref Rect __state, ref Page_CreateWorldParams __instance)
            {
                GUI.BeginGroup(__state);
                float y = 280f;
                Rect baseRect = new Rect(0f, y, 200f, 30f);
                Widgets.Label(baseRect, Translator.Translate("PlanetTypeSettings"));
                Rect EarthRect = new Rect(200f, y, 200f, 30f);
                Rect IceGigantRect = new Rect(200f, y + 40f, 200f, 30f);
                Rect textRect = new Rect(450f, y, __state.width / 2, 300f);
                if (Widgets.RadioButtonLabeled(EarthRect, Translator.Translate("PlanetTypeSettings_Earth"), PlanetType == PlanetTypeEnum.Earth))
                {
                    PlanetType = PlanetTypeEnum.Earth;
                    CurrentPlanet = PlanetsDefOf.Earth;
                }

                if (Widgets.RadioButtonLabeled(IceGigantRect, Translator.Translate("PlanetTypeSettings_IceGigant"), PlanetType == PlanetTypeEnum.IceGigant))
                {
                    PlanetType = PlanetTypeEnum.IceGigant;
                    CurrentPlanet = PlanetsDefOf.IceGigant;
                }

                Widgets.LabelScrollable(textRect, PlanetInfo[(int)PlanetType], ref planetInfoSliderPos, false, false);

                GUI.EndGroup();
            }
        }

        public enum PlanetTypeEnum : int
        {
            Earth = 0,
            IceGigant
        };
        public static PlanetTypeEnum PlanetType = PlanetTypeEnum.Earth;
        public static PlanetDef CurrentPlanet = PlanetsDefOf.Earth;
        public static string[] PlanetInfo = new string[2]
        {
            Translator.Translate("PlanetTypeInfo_Earth"),
            Translator.Translate("PlanetTypeInfo_IceGigant"),
        };

        public RulesOverride()
        {
        }

        public RulesOverride(Game game)
        {

        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<PlanetTypeEnum>(ref PlanetType, "PlanetTypeNum", PlanetTypeEnum.Earth, false);
            Scribe_Defs.Look<PlanetDef>(ref CurrentPlanet, "CurrentPlanetDef");
        }
    }
}

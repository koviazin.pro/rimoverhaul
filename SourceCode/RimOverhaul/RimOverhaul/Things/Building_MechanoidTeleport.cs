﻿using RimWorld;
using Verse;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse.AI.Group;

namespace RimOverhaul.Things
{
    public class Building_MechanoidTeleport : Building
    {
        private int spawnTime = 20000;
        private int animTime = 15;

        private string FramePath = "Things/Buildings/MechanoidTeleport/";
        private Graphic[] Frames;
        private int cycle = 1;
        private Graphic TexMain;

        private Lord lord;

        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);

            LongEventHandler.ExecuteWhenFinished(new System.Action(CreateAnim));
        }
        private void CreateAnim()
        {
            Frames = new Graphic_Single[7];
            for (int i = 0; i < 7; i++)
            {
                Frames[i] = GraphicDatabase.Get<Graphic_Single>(FramePath + (i + 1), this.def.graphicData.Graphic.Shader);
                Frames[i].drawSize = this.def.graphicData.drawSize;
            }
        }

        public override void Tick()
        {
            base.Tick();
            
            if(Find.TickManager.TicksGame % animTime == 0)
            {
                ChangeAnim();
            }
            if(Find.TickManager.TicksGame % spawnTime == 0)
            {
                PawnKindDef kind;
                if (!(from def in DefDatabase<PawnKindDef>.AllDefs where def.RaceProps.IsMechanoid && def.isFighter select def).TryRandomElement(out kind))
                {
                    return;
                }
                IntVec3 center;
                if(!CellFinder.TryFindRandomCellNear(Position, Map, 4, null, out center))
                {
                    return;
                }

                LordJob_AssaultColony lordJob = new LordJob_AssaultColony(Faction.OfMechanoids, false, true, false, false, false);
                lord = LordMaker.MakeNewLord(Faction.OfMechanoids, lordJob, Map, null);
                int count = Rand.Range(1, 4);
                for (int i = 0; i < count; i++)
                {
                    PawnGenerationRequest request = new PawnGenerationRequest(kind, Faction.OfMechanoids, PawnGenerationContext.NonPlayer, -1, true, false, false, false, true, false, 1f, false, true, true, false, false, false, false, null, null, null, null, null, null, null, null);
                    Pawn pawn = PawnGenerator.GeneratePawn(request);
                    GenSpawn.Spawn(pawn, center, Map);
                    lord.AddPawn(pawn);
                }
            }
        }

        private void ChangeAnim()
        {
            if (cycle >= 7)
                cycle = 0;

            TexMain = Frames[cycle];
            TexMain.color = base.Graphic.color;
            cycle++;
        }

        public override void Draw()
        {
           // base.Draw();
            if (this.TexMain != null)
            {
                Matrix4x4 matrix = default(Matrix4x4);
                Vector3 s = new Vector3(3f, 2f, 3f);
                matrix.SetTRS(this.DrawPos + Altitudes.AltIncVect, Rotation.AsQuat, s);
                Graphics.DrawMesh(MeshPool.plane10, matrix, this.TexMain.MatAt(Rotation, null), 0);
            }
        }
    }
}

﻿using RimWorld;
using Verse;
using System.Linq;
using System.Collections.Generic;

namespace RimOverhaul.Things
{
    public class Building_IceWall_Comp : ThingComp
    {
        private DamageInfo damage = new DamageInfo(DamageDefOf.Rotting, 400f);
        private bool canSmelt = false;
    
        public override void Initialize(CompProperties props)
        {
            base.Initialize(props);

            if(parent.Stuff == ThingDefOfLocal.BlocksIce)
            {
                if (parent.AmbientTemperature >= 30f)
                {
                    parent.TakeDamage(damage);
                    return;
                }

                canSmelt = true;
            }
        }
        public override void CompTickRare()
        {
            if (!canSmelt)
            {
                parent.Stuff.tickerType = TickerType.Never;
                return;
            }

            float ambientTemperature = this.parent.AmbientTemperature;
            if(ambientTemperature >= 20f)
            {
                parent.TakeDamage(damage);
                return;
            }
            if (ambientTemperature < 0f)
            {
                return;
            }
            float num = ambientTemperature * 0.5f;
            parent.TakeDamage(new DamageInfo(DamageDefOf.Rotting, num));
        }
    }
}

﻿using Verse;
using RimWorld;
using UnityEngine;
using System.Text;
using System.Collections.Generic;

namespace RimOverhaul
{
    public struct Harvest
    {
        public ThingDef harvest;
        public int count;

        public Harvest(ThingDef harv, int count)
        {
            harvest = harv;
            this.count = count;
        }
    }
    public struct Stage
    {
        public Vector2 Size;
        public float MinGrowth;
        public List<Harvest> HarvestList;

        public Stage(float Growth, Vector2 size, List<Harvest> harvests)
        {
            MinGrowth = Growth;
            Size = size;
            HarvestList = harvests;
        }
    }
    public enum PlantLifeStage : int
    {
        Growing = 0,
        Grown,
        BadTemp
    }

    public abstract class CustomPlant : Building
    {
        public float GrowthSpeed = 0.3f;
        public float CurrentGrowth = 0f;

        public float MinGrowthTemp = -100f;
        public float MaxGrowthTemp = -200f;

        protected List<Stage> Stages = new List<Stage>();
        private bool canHarvest = false;

        public int CurrentStage = 0;
        public PlantLifeStage lifeStage = PlantLifeStage.Growing;

        public bool CanHarvest
        {
            get
            {
                return canHarvest;
            }
        }

        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);

            CurrentStage = 0;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<float>(ref this.CurrentGrowth, "growth", 0f, false);
            Scribe_Values.Look<int>(ref this.CurrentStage, "stage", 0, false);
            Scribe_Values.Look<bool>(ref this.canHarvest, "HarvestableNow", false, false);
        }

        public override void TickLong()
        {
            base.TickLong();

            CheckCond();

            UpdateState();
        }

        private void CheckCond()
        {
            if (AmbientTemperature > MaxGrowthTemp || AmbientTemperature < MinGrowthTemp)
            {
                lifeStage = PlantLifeStage.BadTemp;
            }
            else if(CurrentGrowth >= 100f)
            {
                lifeStage = PlantLifeStage.Grown;
            }
            else
            {
                lifeStage = PlantLifeStage.Growing;
            }
        }
        private void UpdateState()
        {
            if (lifeStage != PlantLifeStage.Growing)
                return;

            CurrentGrowth += GrowthSpeed;

            if (CurrentGrowth >= 100f)
            {
                CurrentGrowth = 100f;
                canHarvest = true;
                def.tickerType = TickerType.Never;

                return;
            }

            if(Stages[CurrentStage].MinGrowth <= CurrentGrowth)
            {
                CurrentStage++;
                base.def.graphicData.drawSize = Stages[CurrentStage].Size;
            }
        }

        public override string GetInspectString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            if(lifeStage == PlantLifeStage.Growing)
            {
                stringBuilder.AppendLine(Translator.Translate("CustomPlant_Growing"));
                stringBuilder.AppendLine(Translator.Translate("CustomPlant_state1").Translate(CurrentGrowth, 100f));
            }else if(lifeStage == PlantLifeStage.Grown)
            {
                stringBuilder.AppendLine(Translator.Translate("CustomPlant_Grow"));
            }
            else
            {
                stringBuilder.AppendLine(Translator.Translate("CustomPlant_BadTemp"));
            }


            return stringBuilder.ToString().TrimEndNewlines();
        }
        public override IEnumerable<Gizmo> GetGizmos()
        {
            if (lifeStage == PlantLifeStage.Grown)
            {
                yield return new Command_Action
                {
                    defaultLabel = "Harvest",
                    action = delegate
                    {

                    }
                };
            }
        }
    }
}

﻿using Verse;
using Harmony;

namespace RimOverhaul
{
    [HarmonyPatch(typeof(RaceProperties)), HarmonyPatch("ResolveReferencesSpecial")]
    public class Harmony_ResolveReferencesSpecial
    {
        public static bool Prefix(RaceProperties __instance)
        {
            if (__instance.useMeatFrom != null)
            {
                if (__instance.useMeatFrom.race == null)
                {
                    __instance.meatDef = __instance.useMeatFrom;
                }
                else
                {
                    __instance.meatDef = __instance.useMeatFrom.race.meatDef;
                }
            }
            if (__instance.useLeatherFrom != null)
            {
                __instance.leatherDef = __instance.useLeatherFrom.race.leatherDef;
            }

            return false;

        }
    }
}

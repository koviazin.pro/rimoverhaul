﻿using RimWorld;
using Verse;
using System.Collections.Generic;
using System.Linq;

namespace RimOverhaul.Things
{
    public class Building_MechanoidTeleport_Generator : Building
    {
        private Thing Teleport;

        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);

            if (!FindHomeAreaCell(map, out IntVec3 result) || Map.areaManager.Home.TrueCount <= 0)
            {
                Destroy(0);
            }

            Teleport = GenSpawn.Spawn(ThingDefOfLocal.MechanoidTeleport, result, map);
        }

        public override void Destroy(DestroyMode mode = DestroyMode.Vanish)
        {
            Teleport.Destroy(0);
            base.Destroy(mode);
        }

        private bool FindHomeAreaCell(Map map, out IntVec3 result)
        {
            List<IntVec3> cells = (from c in map.areaManager.Home.ActiveCells where c.GetRoof(map) != RoofDefOf.RoofRockThick && !c.Fogged(map) select c).ToList();
            if (cells.Count > 0)
            {
                result = cells.RandomElement();
                return true;
            }

            result = IntVec3.Invalid;
            return false;
        }
    }
}

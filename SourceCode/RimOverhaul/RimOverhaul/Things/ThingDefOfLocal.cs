﻿using RimWorld;
using Verse;

namespace RimOverhaul
{
    [DefOf]
    public static class SoundsDefOfLocal
    {
        public static SoundDef Pawn_IceGolem_Wounded;
        public static SoundDef Pawn_IceGolem_Death;
    }

    [DefOf]
    public static class ThingDefOfLocal
    {
        public static ThingDef IceMineable;
        public static ThingDef BlocksIce;
        public static ThingDef ChunkIce;
        public static ThingDef MechanoidTeleport_Generator;
        public static ThingDef MechanoidTeleport;

        public static ThingDef IceGolem;
        public static ThingDef IceMeat;
        public static ThingDef Camouflory;
        public static ThingDef Vulfar;

        public static ThingDef IceMushroom;
    }
}

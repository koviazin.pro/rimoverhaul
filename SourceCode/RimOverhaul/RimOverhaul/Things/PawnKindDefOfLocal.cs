﻿using Verse;
using RimWorld;

namespace RimOverhaul.Things
{
    [DefOf]
    public static class PawnKindDefOfLocal
    {
        public static PawnKindDef IceGolem;
        public static PawnKindDef Camouflory;
        public static PawnKindDef Vulfar;
    }
}

﻿using Harmony;
using RimWorld.Planet;
using RimWorld;
using Verse;

namespace RimOverhaul
{
    [HarmonyPatch("GenerateTileFor"), HarmonyPatch(typeof(WorldGenStep_Terrain))]
    class Harmony_WorldGenStep_Terrain_GenerateFileFor
    {
        public static void Postfix(ref Tile __result)
        {
            UpdateTile(__result);
        }

        public static void UpdateTile(Tile tile)
        {
            if (RulesOverride.PlanetType == RulesOverride.PlanetTypeEnum.Earth)
                return;

            switch (RulesOverride.PlanetType)
            {
                case RulesOverride.PlanetTypeEnum.IceGigant:
                    {

                        int rand = Rand.Range(0, RulesOverride.CurrentPlanet.avaliableHills.Count);
                        bool canSpawn = Rand.Chance(RulesOverride.CurrentPlanet.avaliableHills[rand].freq);
                        if (canSpawn)
                            tile.hilliness = RulesOverride.CurrentPlanet.avaliableHills[rand].hill;
                        else tile.hilliness = RulesOverride.CurrentPlanet.defaultHill;


                        break;
                    }
            }
        }
    }
}

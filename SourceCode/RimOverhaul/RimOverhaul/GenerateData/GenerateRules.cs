﻿using RimWorld.Planet;
using Verse;
using System;
using System.Linq;
using RimWorld;
using System.Collections.Generic;
using Harmony;

namespace RimOverhaul.GenerateData
{
    [HarmonyPatch(typeof(World)), HarmonyPatch("NaturalRockTypesIn")]
    public class Harmony_NaturalRockTypesIn
    {
        public static bool Prefix(int tile, ref IEnumerable<ThingDef> __result)
        {
            __result = SelectRocksThings(tile);

            return false;

        }

        public static IEnumerable<ThingDef> SelectRocksThings(int Tile)
        {
            PlanetDef planet = RulesOverride.CurrentPlanet;
            Rand.PushState();
            Rand.Seed = Tile;

            List<ThingDef> list = planet.stoneResourceList.ToList<ThingDef>();
            int num = Rand.RangeInclusive(planet.stoneResourcesCount.min, planet.stoneResourcesCount.max);
            if (num > list.Count)
            {
                num = list.Count;
            }
            List<ThingDef> list2 = new List<ThingDef>();
            for (int i = 0; i < num; i++)
            {
                ThingDef item = list.RandomElement<ThingDef>();
                list.Remove(item);
                list2.Add(item);
            }
            Rand.PopState();
            return list2;

        }
    }
    [HarmonyPatch(typeof(GenStep_ScatterLumpsMineable)), HarmonyPatch("ChooseThingDef")]
    public class Harmony_GenStep_ScatterLumpsMineable
    {
        public static bool Prefix(ref ThingDef __result)
        {
            if (RulesOverride.PlanetType == RulesOverride.PlanetTypeEnum.Earth || RulesOverride.CurrentPlanet.mineResourceList.Count <= 0)
                return true;

            __result = SelectMineraleThing().resource == null ? ThingDefOf.MineableSteel : SelectMineraleThing().resource;

            return false;

        }
        public static float maxValue = 3.40282347E+38f;
        public static MineResourceData SelectMineraleThing()
        {
            PlanetDef planet = RulesOverride.CurrentPlanet;

            if (planet.mineResourceList == null || planet.mineResourceList.Count <= 0)
                return null;

            return planet.mineResourceList.RandomElementByWeightWithFallback(delegate (MineResourceData d)
            {
                if (d.resource.building == null)
                    return 0f;
                if (d.resource.building.mineableThing != null && d.resource.building.mineableThing.BaseMarketValue > maxValue)
                    return 0f;

                if (d.mineableScatterCommonality == -1)
                {
                    return d.resource.building.mineableScatterCommonality;
                }
                else
                {
                    return d.mineableScatterCommonality;
                }
            });
        }
    }

    [HarmonyPatch(typeof(FactionGenerator)), HarmonyPatch("GenerateFactionsIntoWorld")]
    public class Harmony_FactionGenerator
    {
        public static bool Prefix()
        {
            if (RulesOverride.PlanetType == RulesOverride.PlanetTypeEnum.Earth || RulesOverride.CurrentPlanet.mineResourceList.Count <= 0)
                return true;

            GenerateFactions_Override();

            return false;
        }
        public static void GenerateFactions_Override()
        {
            PlanetDef planet = RulesOverride.CurrentPlanet;

            foreach (FactionDataOnPlanet current in planet.factionsOnPlanet)
            {
                Rand.PushState();
                int num = current.count.max == 1 ? 1 : Rand.RangeInclusive(current.count.min, current.count.max);
                Rand.PopState();
                for (int j = 0; j < num; j++)
                {
                    if (Utility.GenerateChance(current.chancePerCount))
                    {
                        Faction faction = FactionGenerator.NewGeneratedFaction(current.faction);
                        Find.FactionManager.Add(faction);
                    }
                }
            }
        }
    }
}

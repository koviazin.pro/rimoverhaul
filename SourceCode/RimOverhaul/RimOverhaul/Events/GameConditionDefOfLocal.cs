﻿using RimWorld;
using Verse;

namespace RimOverhaul
{
    [DefOf]
    public static class GameConditionDefOfLocal
    {
        public static GameConditionDef SuperHeatWave;
        public static GameConditionDef BoulderMassHit;
        public static GameConditionDef Endlessday;
        public static GameConditionDef LeanAtmosphere;
        public static GameConditionDef RadiationFon;
        public static GameConditionDef NoSun;
        public static GameConditionDef DenseAtmosphere;

        public static GameConditionDef IceStorm;
        public static GameConditionDef HeavyAir;
        public static GameConditionDef WetAir;
        public static GameConditionDef HeavyHail;
    }
}

﻿using RimWorld;

namespace RimOverhaul
{
    [DefOf]
    public static class IncidentDefOfLocal
    {
        public static IncidentDef Supernova;
        public static IncidentDef BoulderMassHit;
        public static IncidentDef SuperHeatWave;
        public static IncidentDef Endlessday;
        public static IncidentDef LeanAtmosphere;
        public static IncidentDef RadiationFon;
        public static IncidentDef NoSun;
        public static IncidentDef DenseAtmosphere;
        public static IncidentDef MechanoidPortal;

        //Events IceGiant
        public static IncidentDef IceStorm;
        public static IncidentDef HeavyAir;
        public static IncidentDef WetAir;
        public static IncidentDef HeavyHail;
        public static IncidentDef Disease_Fibrodysplasia;
    }
}

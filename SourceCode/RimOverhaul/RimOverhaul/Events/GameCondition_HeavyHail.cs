﻿using RimWorld;
using Verse;
using System.Collections.Generic;

namespace RimOverhaul.Events
{
    public class GameCondition_HeavyHail : GameCondition
    {
        private ThingDef ice = ThingDefOfLocal.ChunkIce;
        private int maxMeteors = 10;
        private IntVec3 intVec;

        public override void Init()
        {
            int num = Rand.Range(15, 30);
            Duration = 180000;
            maxMeteors = (Duration / 60000) * num;
        }
        public override void GameConditionTick()
        {
            if (Find.TickManager.TicksGame % 200 == 0)
            {
                if(maxMeteors == 0)
                {
                    End();
                }

                List<Map> affectedMaps = base.AffectedMaps;
                for (int i = 0; i < affectedMaps.Count; i++)
                {
                    if(TryFindCell(out intVec, affectedMaps[i]))
                    {
                        SkyfallerMaker.SpawnSkyfaller(ThingDefOf.MeteoriteIncoming, ThingDefOfLocal.ChunkIce, intVec, affectedMaps[i]);
                        maxMeteors--;
                    }
                }
            }
        }
        private bool TryFindCell(out IntVec3 cell, Map map)
        {
            return CellFinderLoose.TryFindSkyfallerCell(ThingDefOf.MeteoriteIncoming, map, out cell, 10, default(IntVec3), -1, true, true, true, true, false, false);
        }
    }
}

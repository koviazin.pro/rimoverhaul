﻿using RimWorld;
using Verse.AI;
using Verse;

namespace RimOverhaul.Thoughts
{
    public class MentalState_IceMadness : MentalState
    {
        public IntVec3 target;

        public override RandomSocialMode SocialModeMax()
        {
            return RandomSocialMode.Off;
        }

        public override void PostStart(string reason)
        {
            base.PostStart(reason);

            Map map = pawn.Map;
            target = CellFinderLoose.RandomCellWith((IntVec3 c) => c.Standable(map) && !c.Fogged(map) && !c.Roofed(map), map, 1000);
        }
    }
}

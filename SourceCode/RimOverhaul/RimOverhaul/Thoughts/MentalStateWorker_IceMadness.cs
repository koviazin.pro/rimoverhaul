﻿using Verse.AI;
using RimWorld;
using System;
using Verse;

namespace RimOverhaul.Thoughts
{
    public class MentalStateWorker_IceMadness : MentalStateWorker
    {
        public override bool StateCanOccur(Pawn pawn)
        {
            if (!base.StateCanOccur(pawn))
            {
                return false;
            }

            return RulesOverride.PlanetType == RulesOverride.PlanetTypeEnum.IceGigant ? true : false;
        }
    }
}

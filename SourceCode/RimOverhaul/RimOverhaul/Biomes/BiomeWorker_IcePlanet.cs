﻿using RimWorld.Planet;
using RimWorld;
using Verse;

namespace RimOverhaul.Biomes
{
    public class BiomeWorker_IcePlanet : BiomeWorker
    {
        public override float GetScore(Tile tile, int tileID)
        {
            if (RulesOverride.PlanetType == RulesOverride.PlanetTypeEnum.IceGigant)
            {
                float temperatureMax = Rand.Range(-200f, -170f);
                tile.temperature = temperatureMax;

                return 1000f;
            }
            else
            {
                return -100f;
            }
        }
    }
}

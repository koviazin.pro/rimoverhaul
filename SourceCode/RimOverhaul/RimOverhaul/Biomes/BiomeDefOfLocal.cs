﻿using RimWorld;
using Verse;

namespace RimOverhaul
{
    public static class BiomeDefOfLocal
    {
        [DefOf]
        public static class BiomeDefOf
        {
            public static BiomeDef IceGigant_ice;
        }
    }
}

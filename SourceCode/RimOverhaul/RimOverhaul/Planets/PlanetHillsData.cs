﻿using Verse;
using RimWorld.Planet;

namespace RimOverhaul
{
    public class PlanetHillsData
    {
        /*
            Hilliness.Undefined = 0,
            Hilliness.Flat = 1,
            Hilliness.SmallHills = 2,
            Hilliness.LargeHills = 3,
            Hilliness.Mountainous = 4,
            Hilliness.Impassable = 5,
        */
        public Hilliness hill;
        public float freq;
    }
}

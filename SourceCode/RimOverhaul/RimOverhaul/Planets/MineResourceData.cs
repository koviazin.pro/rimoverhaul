﻿using RimWorld;
using Verse;

namespace RimOverhaul
{
    public class MineResourceData
    {
        public ThingDef resource;
        public float mineableScatterCommonality;
    }
}

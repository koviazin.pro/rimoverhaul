﻿using RimWorld;
using Verse;

namespace RimOverhaul
{
    public class FactionDataOnPlanet
    {
        public FactionDef faction;
        public IntRange count;
        public float chancePerCount;
    }
}

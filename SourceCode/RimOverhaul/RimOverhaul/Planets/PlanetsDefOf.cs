﻿using Verse;
using RimWorld;

namespace RimOverhaul
{
    [DefOf]
    public static class PlanetsDefOf
    {
        public static PlanetDef Earth;
        public static PlanetDef IceGigant;
    }
}

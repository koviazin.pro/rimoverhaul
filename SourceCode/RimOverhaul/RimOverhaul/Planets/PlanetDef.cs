﻿using RimWorld;
using Verse;
using System.Collections.Generic;
using RimWorld.Planet;
using System.Linq;

namespace RimOverhaul
{
    public class PlanetDef : Def
    {
        public List<PlanetHillsData> avaliableHills = new List<PlanetHillsData>();
        public Hilliness defaultHill = Hilliness.Flat;
        public IntRange stoneResourcesCount;
        public List<ThingDef> stoneResourceList = new List<ThingDef>();
        public List<MineResourceData> mineResourceList = new List<MineResourceData>();
        public List<FactionDataOnPlanet> factionsOnPlanet = new List<FactionDataOnPlanet>();

        public static PlanetDef Named(string defName)
        {
            return DefDatabase<PlanetDef>.GetNamed(defName, true);
        }
    }
}

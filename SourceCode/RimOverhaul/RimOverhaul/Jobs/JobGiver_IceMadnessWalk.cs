﻿using RimWorld;
using Verse;
using Verse.AI;
using System;
using RimOverhaul.Thoughts;

namespace RimOverhaul.Jobs
{
    public class JobGiver_IceMadnessWalk : JobGiver_Wander
    {
        public JobGiver_IceMadnessWalk()
        {
            this.wanderRadius = 2f;
            this.ticksBetweenWandersRange = new IntRange(300, 600);
            this.locomotionUrgency = LocomotionUrgency.Amble;
            this.wanderDestValidator = new Func<Pawn, IntVec3, IntVec3, bool>(WanderRoomUtility.IsValidWanderDest);
        }

        protected override IntVec3 GetWanderRoot(Pawn pawn)
        {
            MentalState_IceMadness mentalState_icemadness = pawn.MentalState as MentalState_IceMadness;
            if (mentalState_icemadness != null)
            {
                return mentalState_icemadness.target;
            }
            return pawn.Position;
        }
    }
}

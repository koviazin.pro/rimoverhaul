﻿using Harmony;
using System.Reflection;
using Verse;

namespace RimOverhaul
{
    [StaticConstructorOnStartup]
    internal class RimOverhaul : Mod
    {
        public RimOverhaul(ModContentPack content) : base(content)
        {
            HarmonyInstance harmonyInstance = HarmonyInstance.Create("net.funkyshit.rimoverhaul");
            harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
        }

        public void Save()
        {
        }
    }
}

﻿using RimWorld;
using Verse;

namespace RimOverhaul.Hediffs
{
    public class HediffCompProperties_FibroTick : HediffCompProperties
    {
        public HediffCompProperties_FibroTick()
        {
            this.compClass = typeof(HediffComp_FibroTick);
        }
    }
}

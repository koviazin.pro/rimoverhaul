﻿using RimWorld;
using Verse;

namespace RimOverhaul.Hediffs
{
    public class HediffCompProperties_NeuroWormsTick : HediffCompProperties
    {
        public HediffCompProperties_NeuroWormsTick()
        {
            this.compClass = typeof(HediffComp_NeuroWormsTick);
        }
    }
}

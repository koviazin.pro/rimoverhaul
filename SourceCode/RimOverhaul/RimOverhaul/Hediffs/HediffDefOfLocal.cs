﻿using RimWorld;
using Verse;

namespace RimOverhaul
{
    [DefOf]
    public static class HediffDefOfLocal
    {
        public static HediffDef Irradiation;

        public static HediffDef OxygenStarvation;
        public static HediffDef FrostLungs;
        public static HediffDef Fibrodysplasia;
        public static HediffDef NeurofibromatousWorms;
        public static HediffDef NeurofibromatousWormsFinal;
    }
}
